"use client";
import Image from "next/image";
import styles from "./page.module.css";
import Header from "@/Component/Header";
import SectionFirst from "@/Component/SectionFirst";
import CaraouselTeam from "../Component/CaraouselTeam"
export default function Home() {
  return (
    <main className={styles.main}>
      <Header></Header>
      <SectionFirst></SectionFirst>
      <CaraouselTeam></CaraouselTeam>
    </main>
  );
}
