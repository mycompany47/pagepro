"use client";
import React from 'react';
import { Box, Container, Typography, Grid } from '@mui/material';
import styles from "../app/caraousel.module.css"
import FancyCarousel from "react-fancy-circular-carousel";
import 'react-fancy-circular-carousel/FancyCarousel.css';






const Header = () => {


    // the focus element would be the array index of the image currently at focus
    const [focusElement, setFocusElement] = React.useState(0);

    const img = [
        "./image 108.png"
    ]


    const images = [img[0], img[0], img[0]];



    return (<>
        <Box className={styles.HeaderBox}>

            <Box className={styles.HeadingBox}>
                <Typography component='h3' className={styles.Heading}>
                    Meet Our Starclinch Squads
                </Typography>
            </Box>
            <Box className={styles.BoxForImageCircle}>
                <FancyCarousel
                    images={images}
                    setFocusElement={setFocusElement}
                />
                <Box className={styles.BoxforFrame}>
                    <img src='./text.png'></img>
                </Box>
            </Box>

            <Container>
                <Box className={styles.BoxForText}>
                    <Box className={styles.VectorImg}>
                        <img src='./Vector 248.png'></img>
                    </Box>
                    <Grid container className={styles.EachCard}>
                        <Grid xs={12} md={7} className={styles.GridTextBox}>
                            <Box className={styles.TextBox}>
                                <Typography component="h3">
                                    Late Night Maggie Party for the boost
                                </Typography>
                            </Box>
                        </Grid>
                        <Grid xs={12} md={5}>
                            <Box className={styles.BoxForImgFilter}>

                                <img
                                    className={styles.RotateCard}
                                    src='./Frame 1321314913.png'
                                    alt="Rotating Card"
                                />

                            </Box>
                        </Grid>
                    </Grid>

                    <Grid container className={styles.EachCard}>
                        <Grid item xs={12} md={5} >
                            <Box className={styles.BoxForImgFilter}>
                                <img
                                    className={styles.RotateCard}
                                    src='./Frame 1321314913.png'
                                    alt="Rotating Card"
                                />
                            </Box>
                        </Grid>
                        <Grid item xs={12} md={7} className={styles.GridTextBox}>
                            <Box className={styles.TextBox}>
                                <Typography component="h3">
                                    Late Night Maggie Party for the boost
                                </Typography>
                            </Box>

                        </Grid>
                    </Grid>

                    <Grid container className={styles.EachCard}>
                        <Grid item xs={12} md={5} className={styles.GridTextBox}>
                        <Box className={styles.TextBox}>
                                <Typography component="h3">
                                    Late Night Maggie Party for the boost
                                </Typography>
                            </Box>
                        </Grid>
                        <Grid item xs={12} md={7}>
                            <Box className={styles.BoxForImgFilter}>
                                <img
                                    className={styles.RotateCard}
                                    src='./Frame 1321314913.png'
                                    alt="Rotating Card"
                                />
                            </Box>
                        </Grid>
                    </Grid>

                    <Grid container className={styles.EachCard}>
                    <Grid item xs={12} md={7}>
                            <Box className={styles.BoxForImgFilter}>
                                <img
                                    className={styles.RotateCard}
                                    src='./Frame 1321314913.png'
                                    alt="Rotating Card"
                                />
                            </Box>
                        </Grid>
                        <Grid item xs={12} md={5} className={styles.GridTextBox}>
                        <Box className={styles.TextBox}>
                                <Typography component="h3">
                                    Late Night Maggie Party for the boost
                                </Typography>
                            </Box>
                        </Grid>
             
                    </Grid>
                </Box>

            </Container>

            <Container>
                <Box className={styles.BoxForTeam}>
                    <Grid container>
                        <Grid xs={12} md={12}>
                            <Box className={styles.BoxForMission}>
                                <Box className={styles.Card1Mission}>
                                    <img src='./Frame 1321314983.png'></img>
                                </Box>

                                <Box className={styles.BoxForMisionCard}>
                                    <img src='./Frame 1321314983.png'></img>
                                </Box>
                            </Box>
                        </Grid>
                    </Grid>
                </Box>
            </Container>
        </Box>
    </>);
};

export default Header;