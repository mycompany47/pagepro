"use client";
import React from 'react';
import { Box, Container, Typography, Grid } from '@mui/material';
import styles from "../app/Header.module.css"
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';

const Header = () => {
    return (<>
        <Box className={styles.HeaderBox}>
            <Container>
                <Grid container>
                    <Grid xs={12} md={2}>
                        <Box className={styles.Logo} >
                            <Typography component="p">
                                StarClinch
                            </Typography>
                        </Box>
                    </Grid>
                    <Grid xs={12} md={10} className={styles.ListGrid}>
                        <Box className={styles.ListHeader}>
                            <ul className={styles.List}>
                                <li className={styles.celeb}>Book Celebrities <KeyboardArrowDownIcon></KeyboardArrowDownIcon></li>
                                <li>Join Community</li>
                                <li>Post Your Requirements</li>
                            </ul>
                        </Box>
                    </Grid>
                </Grid>
            </Container>

        </Box>
    </>);
};

export default Header;