"use client";
import React from 'react';
import { Box, Container, Typography, Grid } from '@mui/material';
import styles from "../app/Sectionfirst.module.css"
import Image from 'next/image';

const SectionFirst = () => {

    const [isAnimate, setAnimate] = React.useState(false)
    const [imageIndex, setImageIndex] = React.useState(0);
    const [textIndex, setText] = React.useState(0)
    const images = [
        '/arijit.jpeg',
        '/nora.jpeg',
        '/zakir.jpeg'
    ];

    const designation = [
        "Singer",
        "Model",
        "Comedian",
    ]

    function clickToMoveCircle() {
        setAnimate(!isAnimate); // Toggle the state to revert the animation
        setTimeout(() => {
            setImageIndex((prevIndex) => (prevIndex + 1) % images.length);
            setText((prevIndex) => (prevIndex + 1) % designation.length)
        }, 1000)

    }

    React.useEffect(() => {
        if (isAnimate) {
            const timer = setTimeout(() => {
                setAnimate(false); // Reset the state after a delay
            }, 2000); // Adjust the delay time as needed (2 seconds in this case)
            return () => clearTimeout(timer);
        }
    }, [isAnimate]);



    console.log("images[imageIndex]--", images[imageIndex])

    return (<>

        <Box className={styles.BoxForSectionFirst}>
            <Container>
                <Grid container>
                    <Grid xs={12} md={7}>
                        <Box className={styles.ParentCircleBox}>
                            <Box className={styles.GradientCircle}>
                                <Box className={styles.ImageCircle}>
                                    <img src={images[imageIndex]} alt={`Image ${imageIndex + 1}`} onClick={clickToMoveCircle} />
                                    <Box className={isAnimate ? styles.LightCircleAnimate : styles.LightCircle}>
                                    </Box>

                                </Box>
                                <Box className={styles.ArrowBox}>
                                    <img src="/Vector 210.png"></img>
                                </Box>

                                <Box className={styles.DesignationText}>
                                    <Typography component="p">
                                        {designation[textIndex]}
                                    </Typography>
                                </Box>

                            </Box>
                        </Box>
                        <Box className={styles.clickBtn}>
                            <Typography component="p" onClick={clickToMoveCircle}>
                                Click here to view more
                            </Typography>
                        </Box>
                    </Grid>
                    <Grid xs={12} md={4} className={styles.BoxGrid}>
                        <Box className={styles.RightText}>
                            <Typography component="h3">
                                <span className={styles.Graytext}>Choose from</span> 100+ Categories
                                <img src='/explore.png' className={styles.ExplorePng}></img>
                            </Typography>
                        </Box>

                    </Grid>
                </Grid>
                <Box className={styles.BoxForCelebMainContainer}>
                    <Grid container>
                        <Grid xs={12} md={7} className={styles.GridCeleb}>
                            <Box className={styles.BoxForCeleb}>
                                <img src='/Celeb.png'></img>
                            </Box>
                        </Grid>
                        <Grid xs={12} md={5} className={styles.BoxGrid}>
                            <Box className={styles.RightText}>
                                <Typography component="h3">
                                    <span className={styles.Graytext}>Choose from</span> 100+ Categories
                                    <img src='/explore.png' className={styles.ExplorePng}></img>
                                </Typography>
                            </Box>
                        </Grid>
                    </Grid>
                </Box>
            </Container>
        </Box>

    </>)

}

export default SectionFirst;